import {
  HOME_OFFERS_LOADED, CATEGORIES_BY_PARENT_LOADED, BRANDS_LOADED, ATTRIBUTES_LOADED,
  COLORS_LOADED
} from '../actions/actionTypes';

const createSimpleReducer = (EXPECTED_TYPE) => (state = [], {type, payload}) => {
  switch (type) {
    case EXPECTED_TYPE:
      return payload.result;
    default:
      return state
  }
};

export default {
  offers: createSimpleReducer(HOME_OFFERS_LOADED),
  categories: createSimpleReducer(CATEGORIES_BY_PARENT_LOADED),
  brands: createSimpleReducer(BRANDS_LOADED),
  attributes: createSimpleReducer(ATTRIBUTES_LOADED),
  colors: createSimpleReducer(COLORS_LOADED)
}

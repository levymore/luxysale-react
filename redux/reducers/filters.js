import { WOMAN, ORDER_BY } from '../../utils/constants';
import { UPDATE_FILTERS } from '../actions/actionTypes';

const defaultFilter = {
  categories: [], //Objects sample {id: 1, description: 'a'}
  brands: [],
  color: [],
  attributes: [],
  saleMerchant: {},
  clientType: WOMAN,
  countryCode: 'IE',
  offersOrderBy: ORDER_BY.RELEVANCE
};

const filters = (state = defaultFilter, {type, payload}) => {
  switch (type) {
    case UPDATE_FILTERS:
      return payload;
    default:
      return state
  }
};

export default {
  filters
}

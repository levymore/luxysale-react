import React from 'react';
import { Provider } from 'react-redux';
import HomeOffers from './home/home';
import Offers from './offers/offers';
import ContactPage from './contact/contact';
import { Router, Route, browserHistory } from 'react-router';

const Root = ({store}) => (
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={HomeOffers} />
      <Route path="/offers" component={Offers} />
      <Route path="/contact" component={ContactPage} />
    </Router>
  </Provider>
);

export default Root;

import React from 'react';
import { browserHistory } from 'react-router';

class Category extends React.Component {

  constructor(props) {
    super(props);
    const { category } = props;
    this.state = {category};
  }

  updateSelected = () => {
    const { category } = this.state;
    const { categoriesSelected } = this.props;

    const selected = this.isSelected(categoriesSelected, category.id);
    this.state = {category: {...category, selected}};
  };

  isSelected(categories, id) {
    return categories.some((element, index, array) => element.id==id);
  };

  collapse = () => {
    const category = {... this.state.category};
    category.showSubcategories = !category.showSubcategories;
    this.setState({category});
  };

  // TODO build url in other place
  applyCategoryFilter = () => {
    const { category } = this.state;

    browserHistory.push({
      pathname: '/offers',
      query: { categories: category.id, categoriesDescription: category.name, offersOrderBy: 'SORT_BY_RELEVANCE'}
    });
  };

  render() {
    this.updateSelected();

    const { category } = this.state;
    const { categoriesSelected } = this.props;
    const subcategories = category.categoriesDTO.map((subcategory, i) => <Category categoriesSelected={categoriesSelected} category={subcategory} key={i} />);

    return (
      <div>
        <a onClick={this.collapse}> {category.id + ' '+category.name} </a>
        <a onClick={this.applyCategoryFilter}> ({category.selected ? 'X' : '_'}) </a>
        { category.showSubcategories ? subcategories : null }
      </div>
    );
  }
}

export default Category;

import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import Offer from './offer';

class Offers extends React.Component {

  render() {
    const offers = this.props.offers.map((offer, i) => <Offer offer={offer} key={i} />);

    return (
      <div>
        {offers}
      </div>
    );
  }

}

Offers.propTypes = {
  offers: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = ({offers = []}) => ({offers});

export default connect(mapStateToProps)(Offers)

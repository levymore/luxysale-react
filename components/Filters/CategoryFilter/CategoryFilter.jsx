import React from 'react';
import { connect } from 'react-redux';
import Category from './category';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';

class CategoryFilter extends React.Component {

  goback() {
    browserHistory.goBack();
  }

  goToPageProgramaticaly() {
    browserHistory.push({
      pathname: '/contact',
      query: { categories: 123, categories2: 12334}
    });
  }

  render() {
    console.log('CategoryFilter');
    const { filters } = this.props;
    const categories = this.props.categories.map((category, i) => <Category categoriesSelected={filters.categories} category={category} key={i} index={1} />);

    return (
      <div>
        {/*
        Sample:

        <Link
          to={{
            pathname: '/contact',
            query: { categories: 123 }
          }}
          activeStyle={{ color: 'red' }}
        >Direct</Link>

        <a onClick={() => this.goback()} >Go Back</a>
        <a onClick={() => this.goToPageProgramaticaly()} >Programaticaly</a>

        */}

        Categories
        {categories}
      </div>
    );
  }
}

const mapPropsToState = (state) => {
  return {
    categories: state.categories,
    filters: state.filters
  }
};

export default connect(mapPropsToState)(CategoryFilter);

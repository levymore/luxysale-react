export const APP_URL = 'https://luxysale.com';

export const WOMAN =  '1';
export const MEN = '2';

export const ORDER_BY = {
  RELEVANCE: 'SORT_BY_RELEVANCE'
};

import 'babel-polyfill';
import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import FastClick from 'fastclick';
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';
import { createStore, applyMiddleware } from 'redux';
import ApiReducers from './redux/reducers';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger'
import Root from './pages/root';

const container = document.getElementById('container');

/* Integration with chrome extention */
if (window.devToolsExtension && DEBUG) window.devToolsExtension.updateStore(store);

const loggerMiddleware = createLogger();
const middleware = DEBUG ? applyMiddleware(thunkMiddleware, loggerMiddleware) : applyMiddleware(thunkMiddleware);

const reducers = combineReducers({
  routing: routerReducer,
  ...ApiReducers,
});

const store = createStore(
  reducers,
  middleware
);

const render = () => ReactDOM.render(<Root store={store}/>, container);

render();

// Eliminates the 300ms delay between a physical tap
// and the firing of a click event on mobile browsers
// https://github.com/ftlabs/fastclick
FastClick.attach(document.body);

// Enable Hot Module Replacement (HMR)
if (module.hot) module.hot.accept(()=>render());


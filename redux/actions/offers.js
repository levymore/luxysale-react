import { HOME_OFFERS_LOADED } from './actionTypes';

export const clearOffers = () => (dispatch) => dispatch({type: HOME_OFFERS_LOADED, payload:{result: []}});

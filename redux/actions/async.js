import fetch from 'react-native-cancelable-fetch';
import { APP_URL } from '../../utils/constants';
import {
  HOME_OFFERS_LOADED, CATEGORIES_BY_PARENT_LOADED, BRANDS_LOADED, ATTRIBUTES_LOADED,
  COLORS_LOADED, UPDATE_FILTERS, ALL_CATEGORIES_LOADED
} from './actionTypes';

export const fetchPost = (PATH, params, resultDispatch) => {
  const loaded = (result) => ({
    type: resultDispatch,
    payload: {result}
  });
  const URL = `${APP_URL}${PATH}`;

  cancelRequest(resultDispatch);
  return (dispatch, getState) => fetch(URL, {
    method: 'POST',
    headers: new Headers({'Content-Type': 'application/json;charset=UTF-8'}),
    body: JSON.stringify(params)
  })
    .then(response => response.json())
    .then(json => dispatch(loaded(json)));
};

export const fetchGet = (PATH, resultDispatch) => {
  const loaded = (result) => ({
    type: resultDispatch,
    payload: {result}
  });
  const URL = `${APP_URL}${PATH}`;

  cancelRequest(resultDispatch);
  return (dispatch, getState) => fetch(URL, null, resultDispatch)
      .then(response => response.json())
      .then(json => dispatch(loaded(json)));
};

export const cancelRequest = (resultDispatch) => fetch.abort(resultDispatch);

export const loadBrands = (params) => fetchPost('/api/public/v1/brands/fromOffersByFilter/', params, BRANDS_LOADED);
export const loadAttributes = (params) => fetchPost('/api/public/v1/attributes/byOffer/', params, ATTRIBUTES_LOADED);
export const loadColors = (params) => fetchPost('/api/public/v1/offers/colorsFromOffersFiltered/', params, COLORS_LOADED);

export const loadHomeOffers = () => fetchGet('/api/public/v1/offers/home?cacheBuster=1482416159134&categoryId=1&countryCode=IE', HOME_OFFERS_LOADED);
export const loadCategoriesByParent = (parentId) => fetchGet(`/api/public/v1/categories/byParent?parentId=${parentId}`, CATEGORIES_BY_PARENT_LOADED);
export const loadAllCategories = (parentId) => fetchGet(`/api/public/v1/categories?page=0&size=2000`, ALL_CATEGORIES_LOADED); //reducer removed
export const loadOffers = (categories, clientType, countryCode, offersOrderBy, pageNumber, pageSize, smallerParentId) => (
  fetchGet(`/api/public/v1/offers/search?categories=${categories}&clientType=${clientType}&countryCode=${countryCode}&offersOrderBy=${offersOrderBy}&pageNumber=${pageNumber}&pageSize=${pageSize}&smallerParentId=${smallerParentId}`, HOME_OFFERS_LOADED)
);

/*
 URL https://luxysale.com/offers?offersOrderBy=SORT_BY_RELEVANCE&categories=6&categoriesDescription=Clothing

 *f5 offers page

 get https://luxysale.com/api/public/v1/categories/byParent?cacheBuster=1482833995519
 get https://luxysale.com/api/public/v1/shippingCountries?cacheBuster=1482833995519&page=0&size=500
 get https://luxysale.com/api/public/v1/foreign-exchange-rates?cacheBuster=1482833995519&page=0&size=500
 get https://luxysale.com/api/public/v1/categories?cacheBuster=1482833996208&page=0&size=2000
 get https://luxysale.com/api/public/v1/categories/byParent?cacheBuster=1482833996208&parentId=1
 get https://luxysale.com/api/public/v1/offers/colorsFromOffersFiltered?cacheBuster=1482833996208

 post https://luxysale.com/api/public/v1/brands/fromOffersByFilter?cacheBuster=1482833996208
 {"offersOrderBy":"SORT_BY_RELEVANCE","optionsDescription":[],"categories":[6],"brands":[],"colors":[],"clientType":1,"countryCode":"IE","brandsDescription":[],"categoriesDescription":["Clothing"],"saleMerchants":[],"pageNumber":0,"pageSize":20,"smallerParentId":1}

 post https://luxysale.com/api/public/v1/attributes/byOffer?cacheBuster=1482833996208
 {"offersOrderBy":"SORT_BY_RELEVANCE","optionsDescription":[],"categories":[6],"brands":[],"colors":[],"clientType":1,"countryCode":"IE","brandsDescription":[],"categoriesDescription":["Clothing"],"saleMerchants":[],"pageNumber":0,"pageSize":20,"smallerParentId":1}

 post https://luxysale.com/api/public/v1/offers/colorsFromOffersFiltered?cacheBuster=1482833996208
 {"offersOrderBy":"SORT_BY_RELEVANCE","optionsDescription":[],"categories":[6],"brands":[],"colors":[],"clientType":1,"countryCode":"IE","brandsDescription":[],"categoriesDescription":["Clothing"],"saleMerchants":[],"pageNumber":0,"pageSize":20,"smallerParentId":1}

 leva quase 10 segundos pra carregar
 get https://luxysale.com/api/public/v1/offers/search?cacheBuster=1482833996208&categories=6&categoriesDescription=Clothing&clientType=1&countryCode=IE&offersOrderBy=SORT_BY_RELEVANCE&pageNumber=0&pageSize=20&smallerParentId=1
 https://luxysale.com/offers?offersOrderBy=SORT_BY_RELEVANCE&categories=6&categoriesDescription=Clothing

 * */

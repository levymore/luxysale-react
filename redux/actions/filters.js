import { UPDATE_FILTERS } from './actionTypes';

export const syncUrlParamsWithFilters = (params) => (dispatch, getState) => {
  const arrayToObject = (ids, descriptions)=> ids.map((id, index) => ({id, description: descriptions[index]}));

  const categories = arrayToObject(params.categories.split(','), params.categoriesDescription.split(','));
  const { offersOrderBy } = params;

  return dispatch({
    type: UPDATE_FILTERS,
    payload: {
      ...getState(),
      categories,
      offersOrderBy
    }
  });
};

export const clearFilters = () => (dispatch, getState) => {
  const filters = getState().filters;

  dispatch({
    type: UPDATE_FILTERS,
    payload: {
      ...filters,
      categories: [],
      brands: [],
      color: [],
      attributes: [],
      saleMerchant: {},
    }
  });
};

'use strict';

module.exports = {
  'Sample Test' : function (client) {

    let contactUs = '#app > div.fix-footer > a';
    let textContactUs = '#app > div.app-content > div > div';

    client
      .url(client.launchUrl)
      .waitForElementVisible('body', 1000)
      .assert.title('Luxysale - All Designer Sales from 200+ retailers')
      .waitForElementVisible(contactUs, 1000)
      .click(contactUs)
      .assert.containsText(textContactUs, 'A little about us')
      .end();
  }
};

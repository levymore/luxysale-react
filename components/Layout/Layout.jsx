/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import Navbar from '../Navbar';
import Footer from '../Footer';
import css from './Layout.scss'; //Not remove, used by sass to know that need convert this file

class Layout extends React.Component {

  render() {
    return (
      <div>
        <Navbar />
        {/*Page content will be injected into this div*/}
        <div {...this.props} />
        <Footer />
      </div>
    );
  }
}

export default Layout;

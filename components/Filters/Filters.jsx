import React from 'react';
import FiltersSelected from './FiltersSelected';
import BrandFilter from './BrandFilter';
import CategoryFilter from './CategoryFilter';
import PriceFilter from './PriceFilter';
import DiscountFilter from './DiscountFilter';
import ColorFilter from './ColorFilter';
import SizeFilter from './SizeFilter';

class Filters extends React.Component {

  render() {
    return (
      <div>
        <FiltersSelected />

        <CategoryFilter/>
        <BrandFilter/>
        <PriceFilter/>
        <DiscountFilter/>
        <ColorFilter/>
        <SizeFilter/>
      </div>
    );
  }

}

export default Filters;

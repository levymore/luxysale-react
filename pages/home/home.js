import React from 'react';
import Layout from '../../components/Layout';
import Carousel from '../../components/Carousel';
import CategoriesBox from '../../components/CategoriesBox';
import Filters from '../../components/Filters';
import Offers from '../../components/Offers';
import { connect } from 'react-redux'
import { loadHomeOffers, loadCategoriesByParent, loadBrands, loadAttributes, loadColors } from '../../redux/actions/async'
import { clearOffers } from '../../redux/actions/offers'
import { clearFilters } from '../../redux/actions/filters'

class HomePage extends React.Component {

  componentDidMount() {
    const { dispatch, categories, brands, attributes, colors } = this.props;

    dispatch(clearOffers());
    dispatch(clearFilters());
    dispatch(loadHomeOffers());

    //TODO clear all selections when back to home page
    const parentId = 1;
    if(categories.length == 0)
      dispatch(loadCategoriesByParent(parentId));

    const params = {'categories':[1],'clientType':1,'countryCode':'IE'};
    if(brands.length == 0)
      dispatch(loadBrands(params));

    const paramsAttributes = {'categories':[1],'countryCode':'IE'};
    if(attributes.length == 0)
      dispatch(loadAttributes(paramsAttributes));

    const paramsColors = {'categories':[1],'countryCode':'IE'};
    if(colors.length == 0)
      dispatch(loadColors(paramsColors));
  }

  render() {
    return (
      <Layout>
        <h1>Home Page</h1>
        <Carousel/>
        <CategoriesBox/>
        <Filters/>
        <Offers/>
      </Layout>
    );
  }

}

const mapStateToProps = ({offers, categories, brands, attributes, colors}) => ({
  offers,
  categories,
  brands,
  attributes,
  colors
});

export default connect(mapStateToProps)(HomePage)

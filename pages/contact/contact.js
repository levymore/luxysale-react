import React from 'react';
import Layout from '../../components/Layout';

class ContactPage extends React.Component {

  //Test URL
  //http://localhost:3000/contact?categories=64&offersOrderBy=SORT_BY_RELEVANCE&categoriesDescription=Sportswear

  render() {
    const params = this.props.location.query;

    return (
      <Layout>
        <div >A little about us: bla bla bla <br/>
          categories: {params.categories}<br/>
          categories2: {params.categories2}<br/>
        </div>
      </Layout>
    );
  }

}

export default ContactPage;

import React from 'react';

const Offer = ({offer}) => {

  return (
    <div>
      <a href={offer.offerUrl}>
        <img alt={offer.tagline} src={offer.imagePath} width="190" height="220"/>
      </a>
      <br/>
      <span>{offer.description}</span>
    </div>
  );

};

export default Offer;

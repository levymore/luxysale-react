import React from 'react';
import Layout from '../../components/Layout';
import Filters from '../../components/Filters';
import Offers from '../../components/Offers';
import { connect } from 'react-redux';
import { loadCategoriesByParent, loadBrands, loadAttributes, loadColors, loadOffers } from '../../redux/actions/async';
import { clearOffers } from '../../redux/actions/offers';
import { syncUrlParamsWithFilters } from '../../redux/actions/filters';

class OffersPage extends React.Component {

  componentDidMount() {
    this.loadOffersByParams();
  }

  loadOffersByParams() {
    if(this.state && (this.state.search == this.props.location.search)) return;

    this.setState({search: this.props.location.search});

    const { dispatch, categories, brands, attributes, colors } = this.props;

    dispatch(clearOffers());
    dispatch(syncUrlParamsWithFilters(this.props.location.query));

    //TODO build page by params of URL
    const categoriesFilter = [this.props.location.query.categories], clientType = 1, countryCode = 'IE',
      offersOrderBy = this.props.location.query.offersOrderBy, pageNumber = 0, pageSize = 20, smallerParentId = 1;
    dispatch(loadOffers(categoriesFilter, clientType, countryCode, offersOrderBy, pageNumber, pageSize, smallerParentId));

    const parentId = 1;
    if(categories.length == 0)
      dispatch(loadCategoriesByParent(parentId));

    const params = {'categories':[1],'clientType':1,'countryCode':'IE'};
    if(brands.length == 0)
      dispatch(loadBrands(params));

    const paramsAttributes = {'categories':[1],'countryCode':'IE'};
    if(attributes.length == 0)
      dispatch(loadAttributes(paramsAttributes));

    const paramsColors = {'categories':[1],'countryCode':'IE'};
    if(colors.length == 0)
      dispatch(loadColors(paramsColors));
  }

  componentDidUpdate() {
    this.loadOffersByParams();
  }

  render() {
    return (
      <Layout >
        {/* sort by */}
        <h1>Offers Page</h1>
        <Filters/>
        <Offers/>
      </Layout>
    );
  }

}

const mapStateToProps = ({offers, categories, brands, attributes, colors}) => ({
  offers,
  categories,
  brands,
  attributes,
  colors
});

export default connect(mapStateToProps)(OffersPage)
